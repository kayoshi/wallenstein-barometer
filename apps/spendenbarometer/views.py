from django.http import HttpResponse
# some_app/views.py
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, CreateView
from .forms import BuchungForm, RawBuchungForm
from apps.spendenbarometer.models import Konto, Buchung


class ZusagenView(CreateView):
    model = Buchung
    form_class = BuchungForm
    template_name = "zusagen.html"
    success_url = reverse_lazy("zusagen")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ZusGen = Konto.objects.get(id=1)
        ZusDk = Konto.objects.get(id=2)
        context.update({
            'ZusGen': ZusGen,
            'ZusDk': ZusDk
        })
        return context



    # Condition KontoType (two bookings for each account possible!)
    # def form_valid(self, form):
    #     new_booking = form.save(commit=False)
    #     reg_number = new_booking.reg_number
    #     amount = new_booking.amount
    #     print(new_booking.reg_number)
    #     obj, created = Buchung.objects.update_or_create(
    #         reg_number=reg_number,
    #         defaults={'amount': amount},
    #     )
    #     obj.save()

    # overwrite amount if reg_number already exists:
    # def form_valid(self, form):
    #     new_booking = form.save(commit=False)
    #     all_bookings = Buchung.objects.all()
    #     for obj in all_bookings:
    #         count = 0
    #         if obj.reg_number == new_booking.reg_number:
    #             print('Object exists')
    #             count += 1
    #     print(count)
    #     if count == 0:
    #         return super().form_valid(form)


# def buchung_create_view(request):
#     my_form = BuchungForm()
#     if request.method == 'POST':
#         my_form = BuchungForm(request.POST)
#         if my_form.is_valid():
#             Buchung.objects.create(**my_form.cleaned_data)
#         else:
#             print ("object was not created")
#     context = {
#         'form': my_form
#     }
#     return render(request, 'form.html', context)

# def buchung_create_view(request):
#     form = BuchungForm(request.POST or None)
#     if form.is_valid():
#         form.save()
#     context = {
#         'form': form
#     }
#     return render(request, 'form.html', context)


def index(request):
    accounts = Konto.objects.all()
    context = {
        'konten': accounts
    }
    return render(request, 'base.html', context)


def zusagen_view(request):
    ZusGen = Konto.objects.get(id=1)
    ZusDk = Konto.objects.get(id=2)
    # ZusGen.calc_balance()
    # ZusDk.calc_balance()
    context = {
        'ZusGen': ZusGen,
        'ZusDk': ZusDk
    }
    return render(request, 'zusagen.html', context)


def konto_view(request):
    return render(request, 'konto.html')


def baukosten_view(request):
    return render(request, 'baukosten.html')
