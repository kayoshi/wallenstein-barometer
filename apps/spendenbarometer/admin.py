from csvexport.actions import csvexport
from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import Konto
from .models import Buchung

class BuchungAdmin(ModelAdmin):
    list_display = ["reg_number", "name", "amount"]
    actions = [csvexport]

    CSV_EXPORT_REFERENCE_DEPTH = 3
    CSV_EXPORT_EMPTY_VALUE = ''
    CSV_EXPORT_DELIMITER = ','
    CSV_EXPORT_ESCAPECHAR = ''
    CSV_EXPORT_QUOTECHAR = '"'
    CSV_EXPORT_DOUBLEQUOTE = True
    CSV_EXPORT_LINETERMINATOR = r'\n'
    CSV_EXPORT_QUOTING = 'QUOTE_ALL'


admin.site.register(Konto)
admin.site.register(Buchung, BuchungAdmin)

