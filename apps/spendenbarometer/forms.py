from django import forms
from .models import Buchung, Konto


class BuchungForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['konto'].queryset = Konto.objects.filter(is_public=True)

    class Meta:
        model = Buchung

        fields = [
            'konto',
            'name',
            'reg_number',
            'amount'
        ]


class RawBuchungForm(forms.Form):
    # CHOICES = [
    #     Konto.objects.filter(id=1)
    # ]
    name = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "mb-6",
                "label": "Name Mitglied/Externe"
            }))
    reg_number = forms.IntegerField()
    amount = forms.IntegerField()
    konto = forms.ModelChoiceField(queryset=Konto.objects.filter(is_public=True))
