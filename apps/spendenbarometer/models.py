from django.db import models
from django.db.models import Sum


class Konto(models.Model):
    name = models.CharField(max_length=50)
    balance = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    is_public = models.BooleanField(default=False)

    def calc_balance(self):
        query = Buchung.objects.filter(konto=self).aggregate(Sum('amount'))
        return query['amount__sum'] or 0

    def fill_height(self):
        return str(self.calc_balance() / 116000)

    def fill_height_sm(self):
        return str(self.calc_balance() / 40000)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name_plural = "Konten"


class Buchung(models.Model):
    konto = models.ForeignKey(Konto,  on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    reg_number = models.IntegerField(default=0)
    amount = models.DecimalField(decimal_places=2, max_digits=12, default=0)

    class Meta:
        verbose_name_plural = "Buchungen"

    def __str__(self):
        return str(self.reg_number)
