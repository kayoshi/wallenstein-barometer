import {defineConfig} from "vite";
import {nodeResolve} from '@rollup/plugin-node-resolve';

export default defineConfig({
  plugins: [
    nodeResolve(),
  ],
  base: "/static/",
  build: {
    outDir: "dist",
    assetsDir: ".",
    manifest: true,
    rollupOptions: {
      input: {
        "main": "src/main.js"
      },
    },
  },
});
