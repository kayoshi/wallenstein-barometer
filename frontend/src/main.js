// Styles
import "./scss/style.scss";
import "./js/confetti.js";

// eslint-disable-next-line no-undef
let confetti = new Confetti('partytime');

confetti.setCount(75);
confetti.setSize(1);
confetti.setPower(25);
confetti.setFade(false);
confetti.destroyTarget(true);
