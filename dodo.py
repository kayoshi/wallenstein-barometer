#!/usr/bin/env python3

from glob import glob

import environ

env = environ.Env()
env.read_env(".env")

DOIT_CONFIG = {"default_tasks": ["update"], "verbosity": 2}


def task_prepare():
    return {
        "task_dep": ["collectstatic", "compilemessages"],
        "actions": [],
    }


def task_update():
    return {
        "task_dep": ["collectstatic", "compilemessages", "migrate"],
        "actions": [],
    }


def task_init_setup():
    return {"task_dep": ["update"], "actions": ["./manage.py init_setup"]}


def task_reset_db():
    db = env.db()
    actions = []

    if "postgis" in db["ENGINE"] or "postgresql" in db["ENGINE"]:
        actions = [
            f"sudo -u postgres psql -c 'drop database {db['NAME']}' || true",
            f"sudo -u postgres psql -c 'create database {db['NAME']} with owner {db['USER']}'",
        ]
        if "postgis" in db["ENGINE"]:
            actions += [
                f"sudo -u postgres psql {db['NAME']} -c 'create extension postgis'"
            ]

    elif db["ENGINE"] == "django.db.backends.sqlite3":
        actions = [f"rm -f {db['NAME']}"]

    return {"actions": actions}


def task_reset_db_mac():
    db = env.db()
    actions = []

    if "postgis" in db["ENGINE"] or "postgresql" in db["ENGINE"]:
        actions = [
            f"psql -U postgres -c 'drop database {db['NAME']}' || true",
            f"psql -U postgres -c 'create database {db['NAME']} with owner postgres'",
        ]
        if "postgis" in db["ENGINE"]:
            actions += [f"psql -U postgres {db['NAME']} -c 'create extension postgis'"]

    elif db["ENGINE"] == "django.db.backends.sqlite3":
        actions = [f"rm -f {db['NAME']}"]

    return {"actions": actions}


def task_sync_staging():
    db = env.db()
    actions = []

    actions = [
        f"psql -c 'drop database {db['NAME']}'",
        f"psql -c 'create database {db['NAME']}'",
        f"ssh zoe@sinndemo.de 'pg_dump zoe | bzip2' | bunzip2 | psql zoe",
        f"rsync zoe@sinndemo.de:htdocs/media/ media/ -av",
    ]

    return {"actions": actions}


#############################
def task_collectstatic():
    return {
        "task_dep": ["npm_build"],
        "actions": ["./manage.py collectstatic --noinput"],
    }


def task_compilemessages():
    for pofile in glob("**/LC_MESSAGES/*.po", recursive=True):
        mofile = pofile[:-2] + "mo"
        yield {
            "name": pofile,
            "file_dep": [pofile],
            "targets": [mofile],
            "actions": [f"msgfmt -o {mofile} {pofile}"],
            "clean": True,
        }


def task_migrate():
    return {"actions": ["./manage.py migrate --noinput"]}


def task_npm_build():
    return {
        "targets": ["node_modules/"],
        "actions": ["cd frontend && npm ci", "cd frontend && npm run build"],
    }
