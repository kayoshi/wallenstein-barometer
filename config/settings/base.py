import environ

BASE_DIR = environ.Path(__file__) - 3  # type: environ.Path

env = environ.Env()
env.read_env(BASE_DIR(".env"))

# Internationalization
TIME_ZONE = "Europe/Berlin"
LANGUAGE_CODE = "de-de"
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
SITE_PROTOCOL = "https"
SITE_TITLE = "GSP Spendenbarometer"

LOCALE_PATHS = (BASE_DIR("config/locale"),)

ROOT_URLCONF = "config.urls"
WSGI_APPLICATION = "config.wsgi.application"

# Email Config
EMAIL_CONFIG = env.email_url("DJANGO_EMAIL_URL", default="consolemail://")
vars().update(EMAIL_CONFIG)

ALLOWED_HOSTS = env.list("DJANGO_ALLOWED_HOSTS", default=["127.0.0.1", "localhost"])

INTERNAL_IPS = [
    "127.0.0.1",
]

DATABASES = {"default": env.db("DATABASE_URL")}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    "apps.spendenbarometer",
    "django_vite",
    "csvexport",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "csp.middleware.CSPMiddleware",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.media",
            ],
        },
    },
]

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        "OPTIONS": {
            "min_length": 6,
        },
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
]

# Static files
MEDIA_ROOT = BASE_DIR("media")
MEDIA_URL = "/media/"
FILE_UPLOAD_PERMISSIONS = 0o2755
FILE_UPLOAD_MAX_MEMORY_SIZE = 50000000

STATIC_ROOT = BASE_DIR("static-collected")
STATIC_URL = "/static/"
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "django.contrib.staticfiles.finders.FileSystemFinder",
]

DJANGO_VITE_ASSETS_PATH = BASE_DIR("frontend", "dist")
DJANGO_VITE_DEV_MODE = False

STATICFILES_DIRS = (
    BASE_DIR("static"),
    DJANGO_VITE_ASSETS_PATH,
)

# Auth / Login
ADMINS = (("Sinnwerkstatt Admin", "webmaster@sinnwerkstatt.com"),)
MANAGERS = ADMINS
# DEFAULT_FROM_EMAIL = env("DJANGO_DEFAULT_FROM_EMAIL", default="reisedossiers@goethe.de")

# CSP
CSP_DEFAULT_SRC = ("'self'", "'unsafe-inline'")
CSP_FONT_SRC = ("'self'", "data:")

TAILWIND_APP_NAME = 'apps.theme'
