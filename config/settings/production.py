import random
import string

from environ import ImproperlyConfigured

from .base import *  # noqa

DEBUG = env.bool("DJANGO_DEBUG", default=False)

# Email Config
EMAIL_CONFIG = env.email_url("DJANGO_EMAIL_URL", default="smtp://localhost")
vars().update(EMAIL_CONFIG)

try:
    SECRET_KEY = env("DJANGO_SECRET_KEY")
except ImproperlyConfigured:
    SECRET_KEY = "".join(
        [
            random.SystemRandom().choice(
                "{}{}{}".format(string.ascii_letters, string.digits, "+-:$;<=>?@^_~")
            )
            for i in range(63)
        ]
    )
    with open(".env", "a") as envfile:
        envfile.write("DJANGO_SECRET_KEY={}\n".format(SECRET_KEY))

# Raven
try:
    INSTALLED_APPS += ("sentry_sdk",)
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration

    sentry_sdk.init(
        dsn=env("DJANGO_SENTRY_DSN"),
        integrations=[DjangoIntegration()],
        send_default_pii=True,
    )

except ImproperlyConfigured:
    print("Watch out, there is no sentry dsn, so no sentry-support!")


CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True  # default anyway
SESSION_COOKIE_SECURE = True
#
# SESSION_COOKIE_AGE = 3075840000  # 100 years
