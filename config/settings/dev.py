from .base import *  # noqa

DEBUG = env.bool("DJANGO_DEBUG", default=True)
SECRET_KEY = env("DJANGO_SECRET_KEY", default="CHANGEME")
DJANGO_VITE_DEV_MODE = True

try:
    import django_extensions

    INSTALLED_APPS += ["django_extensions"]

except ModuleNotFoundError:
    pass

# try:
#     import debug_toolbar
#
#     INSTALLED_APPS += ["debug_toolbar"]
#     MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]

except ModuleNotFoundError:
    pass

CSP_DEFAULT_SRC = ("'self'", "ws://127.0.0.1:*", "ws://localhost:*")
CSP_SCRIPT_SRC = (
    "'self'",
    "'unsafe-inline'",
    "'unsafe-eval'",
    "http://127.0.0.1:*",
    "http://localhost:*",
)
CSP_STYLE_SRC = ("'self'", "'unsafe-inline'")

